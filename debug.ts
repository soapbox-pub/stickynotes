import { Stickynotes } from './mod.ts';

/** Drop-in replacement for `debug-js`. */
export default function (namespace: string): Console['debug'] {
  const sticky = new Stickynotes(namespace);
  return (...data: any[]) => sticky.debug(...data);
}
