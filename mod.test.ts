import { assertEquals } from 'https://deno.land/std@0.213.0/assert/mod.ts';
import { stub } from 'https://deno.land/std@0.213.0/testing/mock.ts';

import { Stickynotes } from './mod.ts';

Deno.test('Respects `DEBUG` value', () => {
  const log = stub(console, 'log');

  const sticky = new Stickynotes('yolo:fam');

  assertEquals(log.calls.length, 0);

  sticky.log('log');
  assertEquals(log.calls.length, 0);

  Deno.env.set('DEBUG', 'yolo:*');
  sticky.log('log');
  assertEquals(log.calls.length, 1);

  Deno.env.set('DEBUG', 'yolo:fam');
  sticky.log('log');
  assertEquals(log.calls.length, 2);

  Deno.env.set('DEBUG', '');
  sticky.log('log');
  assertEquals(log.calls.length, 2);

  Deno.env.set('DEBUG', '*');
  sticky.log('log');
  assertEquals(log.calls.length, 3);

  log.restore();
});

Deno.test('Respects `LOG_LEVEL`', () => {
  const log = stub(console, 'log');
  const warn = stub(console, 'warn');

  const sticky = new Stickynotes('yolo:fam');

  assertEquals(log.calls.length, 0);
  assertEquals(warn.calls.length, 0);

  sticky.log('log');
  assertEquals(log.calls.length, 1);

  sticky.warn('warn');
  assertEquals(warn.calls.length, 1);

  Deno.env.set('LOG_LEVEL', 'warn');

  sticky.log('log');
  assertEquals(log.calls.length, 1);

  sticky.warn('warn');
  assertEquals(warn.calls.length, 2);

  Deno.env.set('LOG_LEVEL', 'log');

  sticky.log('log');
  assertEquals(log.calls.length, 2);

  Deno.env.set('LOG_LEVEL', 'error');

  sticky.warn('warn');
  assertEquals(warn.calls.length, 2);

  log.restore();
});
