# Stickynotes

A dead-simple logger for Deno, inspired by [debug-js](https://github.com/debug-js/debug).

## Usage

```ts
import { Stickynotes } from 'jsr:@soapbox/stickynotes';

const console = new Stickynotes('app:main');

console.log('Hello, world!');
console.error('This is an error message');
console.warn('This is a warning message');
console.info('This is an info message');
console.debug('This is a debug message');

console.assert(true, 'The full `Console` interface is available');
```

## Features

- Implements the full [`Console`](https://developer.mozilla.org/en-US/docs/Web/API/console) interface.
- Supports log levels (`error`, `warn`, `info`, `debug`, `log`) with the `LOG_LEVEL` environment variable.
- Supports namespaces using the `DEBUG` environment variable.
- Customizable message format.

## Options

The `Stickynotes` constructor accepts an optional `options` object as its second argument. The following options are available:

- `color` (`string`) - Hex color code (eg `#ff0000`) to use for the log messages. Defaults to a random color.
- `console` (`Console`) - The console object to use. Defaults to `console`.
- `env` (`Deno.Env`) - The environment object to use. Defaults to `Deno.env`.
- `formatter` (`(sticky: Stickynotes, data: any[]) => string`) - A function that formats the log message. Defaults to `Stickynotes.formatter`.

### Environment Variables

If `env` is passed to the `Stickynotes` constructor, the following environment variables are used:

- `DEBUG` - A comma-separated list of debug namespaces to enable. It is the same format as `debug-js`.
- `LOG_LEVEL` - One of `error`, `warn`, `info`, `debug`, or `log`. Defaults to `debug`.

## Drop-in replacement for `debug-js`

It's possible to import `Stickynotes` as a drop-in replacement for `debug-js`:

```ts
import Debug from 'jsr:@soapbox/stickynotes/debug';

const debug = Debug('app:main');

debug('Hello, world!');
```

Internally it uses `sticky.debug`, so be aware of the `LOG_LEVEL`.

## License

This project is licensed under the MIT License. See the [LICENSE](./LICENSE) file for details.
