import colors from './colors.json' with { type: 'json' };

export interface StickynotesOpts {
  color?: string;
  console?: Console;
  env?: Deno.Env;
  formatter?: (sticky: Stickynotes, data: any[]) => any[];
}

export class Stickynotes implements Console {
  readonly namespace: string;
  readonly console: Console;
  readonly color: string;
  readonly env: Deno.Env;

  private readonly formatter: (sticky: Stickynotes, data: any[]) => any[];

  constructor(namespace: string, opts: StickynotesOpts = {}) {
    this.namespace = namespace;
    this.console = opts.console ?? globalThis.console;
    this.color = opts.color ?? Stickynotes.color(namespace);
    this.env = opts.env ?? Deno.env;
    this.formatter = opts.formatter ?? Stickynotes.formatter;
  }

  private format(data: any[]): any[] {
    return this.formatter(this, data);
  }

  get enabled(): boolean {
    const DEBUG = this.env.get('DEBUG') ?? '';
    return Stickynotes.enabled(this.namespace, DEBUG);
  }

  get level(): number {
    const LOG_LEVEL = this.env.get('LOG_LEVEL') ?? 'debug';
    return Stickynotes.level(LOG_LEVEL);
  }

  assert(condition?: boolean, ...data: any[]): void {
    if (this.enabled && this.level >= 0) {
      return this.console.assert(condition, ...this.format(data));
    }
  }

  clear(): void {
    if (this.enabled) {
      return this.console.clear();
    }
  }

  count(label?: string): void {
    if (this.enabled && this.level >= 5) {
      return this.console.count(label);
    }
  }

  countReset(label?: string): void {
    if (this.enabled) {
      return this.console.countReset(label);
    }
  }

  debug(...data: any[]): void {
    if (this.enabled && this.level >= 4) {
      return this.console.debug(...this.format(data));
    }
  }

  dir(item?: any, options?: any): void {
    if (this.enabled && this.level >= 4) {
      return this.console.dir(item, options);
    }
  }

  dirxml(...data: any[]): void {
    if (this.enabled && this.level >= 4) {
      return this.console.dirxml(...this.format(data));
    }
  }

  error(...data: any[]): void {
    if (this.enabled && this.level >= 0) {
      return this.console.error(...this.format(data));
    }
  }

  group(...data: any[]): void {
    if (this.enabled) {
      return this.console.group(...this.format(data));
    }
  }

  groupCollapsed(...data: any[]): void {
    if (this.enabled) {
      return this.console.groupCollapsed(...this.format(data));
    }
  }

  groupEnd(): void {
    if (this.enabled) {
      return this.console.groupEnd();
    }
  }

  info(...data: any[]): void {
    if (this.enabled && this.level >= 2) {
      return this.console.info(...this.format(data));
    }
  }

  log(...data: any[]): void {
    if (this.enabled && this.level >= 3) {
      this.console.log(...this.format(data));
    }
  }

  table(tabularData?: any, properties?: string[]): void {
    if (this.enabled && this.level >= 3) {
      return this.console.table(tabularData, properties);
    }
  }

  time(label?: string): void {
    if (this.enabled && this.level >= 5) {
      return this.console.time(label);
    }
  }

  timeEnd(label?: string): void {
    if (this.enabled && this.level >= 5) {
      return this.console.timeEnd(label);
    }
  }

  timeLog(label?: string, ...data: any[]): void {
    if (this.enabled && this.level >= 5) {
      return this.console.timeLog(label, ...data);
    }
  }

  trace(...data: any[]): void {
    if (this.enabled && this.level >= 5) {
      return this.console.trace(...this.format(data));
    }
  }

  warn(...data: any[]): void {
    if (this.enabled && this.level >= 1) {
      return this.console.warn(...this.format(data));
    }
  }

  timeStamp(label?: string): void {
    if (this.enabled && this.level >= 5) {
      return this.console.timeStamp(label);
    }
  }

  profile(label?: string): void {
    if (this.enabled && this.level >= 5) {
      return this.console.profile(label);
    }
  }

  profileEnd(label?: string): void {
    if (this.enabled && this.level >= 5) {
      return this.console.profileEnd(label);
    }
  }

  /** Select the color for this namespace from the list. */
  static color(namespace: string): string {
    let hash = 0;

    for (let i = 0; i < namespace.length; i++) {
      hash = ((hash << 5) - hash) + namespace.charCodeAt(i);
      hash |= 0; // Convert to 32bit integer
    }

    return colors[Math.abs(hash) % colors.length];
  }

  /** Default message formatter. */
  static formatter(sticky: Stickynotes, data: any[]): any[] {
    return [`  %c${sticky.namespace}`, `color: ${sticky.color}`, ...data];
  }

  /** Parse `DEBUG` string format into a list of namespaces. */
  static parse(DEBUG: string): string[] {
    return DEBUG.split(/[\s,]+/).filter(Boolean);
  }

  /** Check if the namespace is enabled by the `DEBUG` string. */
  static enabled(namespace: string, DEBUG: string): boolean {
    if (DEBUG === '*') return true;

    const namespaces = Stickynotes.parse(DEBUG);
    const pattern = new RegExp('^' + namespaces.map((ns) => ns.replace(/\*/g, '.*')).join('|') + '$');

    return pattern.test(namespace);
  }

  /** Parse the log level into a number. */
  static level(LOG_LEVEL: string): number {
    switch (LOG_LEVEL) {
      case 'error':
        return 0;
      case 'warn':
        return 1;
      case 'info':
        return 2;
      case 'log':
        return 3;
      case 'debug':
        return 4;
      case 'trace':
        return 5;
      default:
        return NaN;
    }
  }
}
